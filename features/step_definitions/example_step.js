const { Given, When, Then } = require('cucumber');
const chai = require('chai');
const expect = chai.expect;
  
const generalPO = require('../support/pages/General.po');
const home = require('../support/pages/Home.po');

Given('I enter into the example page', async function () {
  await testController
    .navigateTo(this.attach.testData.urls.url);
});


When('I insert name in the fild you name {string}', async function (nombre) {
  console.log(nombre)
  await testController
    //.typeText(home.HomePO.getYourNameInput(), this.attach.testData.datosPrueba.name)
    .typeText(home.HomePO.getYourNameInput(), nombre)
});
    


When('Insert what you think {string}', async function (think) {
  await testController
    .click(home.HomePO.getTriedTestcafe())
    .expect(home.HomePO.getTriedTestcafe().checked).ok()
    .typeText(home.HomePO.getCommentsInput(), think)
});


When('press button Submit', async function () {
  await testController
    .click(home.HomePO.getSubmitButton())
});


Then('display message {string}', async function (message) {
  await testController
    .expect(home.HomePO.getMessageLabel().innerText).contains(message)
});
