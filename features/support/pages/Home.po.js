const {Selector} = require('testcafe');

function select(selectorid) {
    return Selector(selectorid).with({boundTestRun: testController});
}

exports.HomePO = {
  getYourNameInput: () => {
    return select('#developer-name');
  },

  getTriedTestcafe: () => {
    return select('#tried-test-cafe') 
  },

  getCommentsInput: () => {
    return select('#comments');
  }, 
  
  getSubmitButton: () => {
    return select('#submit-button');
  },

  getMessageLabel: () => {
    return select('#article-header');
  }
  
};