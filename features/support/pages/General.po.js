const {Selector} = require('testcafe');
 
function select(selector) {
  return Selector(selector).with({boundTestRun: testController});
}
  
exports.GeneralPage = {
  url: function(){
    return (this.attach.testData.urls.url);
  }
};